import asyncio
import json

from . import sheets
from .sheets import assert_cell_equal, assert_cell_string

SHEET_ID = "17_vLQ8Rv5bYxWvfsuhCNZTzZMUg0hXbYqJ9ts7nOa-k"
# SHEET_ID = "1sZvZLrfOkc6QUv-aBY1Z92voFwtWdJMhSLC7J-7FUH8"


async def update_scores(config, sheet_id, points_needed_to_win=10):
    sheet = await sheets.load_sheet_contents(sheet_id, config)
    sheet_names = sheet.get_sheets()

    player_scores = {}

    for sheet_name in sheet_names:
        if "match" not in sheet_name.lower():
            continue
        for table_info in sheets.get_qualifier_game_info(sheet, sheet_name):
            print(f"processing table {sheet_name} {table_info.name}")
            total_score = 0
            all_names = []
            for player in table_info.players:

                assert (
                    isinstance(player.score, float) or isinstance(player.score, int) or player.score is None
                ), f"Invalid player score: {player.score} for player {player.discord_name} on sheet {sheet_name}"

                if isinstance(player.score, float) or isinstance(player.score, int):
                    all_names.append(
                        (player.discord_name, player.colonist_name, player.score)
                    )
                    total_score += player.score

                else:
                    all_names.append((player.discord_name, player.colonist_name, 0))

            print(total_score)
            sorted_vps = sorted((i[2] for i in all_names), reverse=True)

            for name, colonist_name, score in all_names:
                win = score >= points_needed_to_win

                if name not in player_scores:
                    player_scores[name] = {
                        "colonist_name": colonist_name,
                        "wins": 0,
                        "points": 0,
                        "percent": 0,
                        "table_points": 0,
                        "point_diff": 0,
                        "opponents": [],
                        "nrof_games": 0,
                        "games": set(),
                        "uncompleted_games": set(),
                    }

                if score == sorted_vps[0]:
                    diff = score - sorted_vps[1]
                else:
                    diff = score - sorted_vps[0]

                player_info = player_scores[name]
                if score == 0:
                    player_info["uncompleted_games"].add(sheet_name)
                else:
                    player_info["wins"] += win
                    player_info["points"] += score
                    player_info["percent"] += score / total_score
                    player_info["table_points"] += total_score
                    player_info["point_diff"] += diff
                    player_info["nrof_games"] += 1
                    player_info["games"].add(sheet_name)

                    print(player_info["percent"])

                player_info["opponents"].extend(i[0] for i in all_names if i[0] != name)

    new_player_info = []
    for name, player in player_scores.items():
        new_player_info.append(
            {
                "discord_name": name,
                "colonist_name": player["colonist_name"],
                "wins": player["wins"],
                "points": player["points"],
                "percent": player["percent"] / max(1, player["nrof_games"]),
                "table_points": player["table_points"],
                "point_diff": player["point_diff"],
                "avg_vp_op": sum(
                    player_scores[i]["points"] / max(1, len(player_scores[i]["games"]))
                    for i in player["opponents"]
                )
                / max(1, len(player["opponents"])),
                "total_win_opponent": sum(
                    player_scores[i]["wins"] for i in player["opponents"]
                ),
                "games": player["games"],
                "uncompleted_games": player["uncompleted_games"],
            }
        )

    new_player_info.sort(key=lambda i: i["total_win_opponent"], reverse=True)
    new_player_info.sort(key=lambda i: i["point_diff"], reverse=True)
    new_player_info.sort(key=lambda i: i["table_points"], reverse=True)
    new_player_info.sort(key=lambda i: i["percent"], reverse=True)
    new_player_info.sort(key=lambda i: i["points"], reverse=True)
    new_player_info.sort(key=lambda i: i["wins"], reverse=True)

    for player in new_player_info:
        opponents = player_scores[player["discord_name"]]["opponents"]
        total_rank = 0
        for opponent in opponents:
            total_rank += next(
                i
                for i, j in enumerate(new_player_info)
                if j["discord_name"] == opponent
            )

        player["avg_rank_opponents"] = total_rank // len(opponents)

    scores_sheet_name = next(i for i in sheet.get_sheets() if "ranking" in i.lower())
    scores_sheet_id = sheet.get_sheet_id(scores_sheet_name)

    await sheets.write_results_to_sheet(
        sheet_id,
        scores_sheet_id,
        new_player_info,
        [i for i in sheet_names if "match" in i.lower()],
        config,
    )


if __name__ == "__main__":
    with open("../secret.json") as f:
        config = json.load(f)

    asyncio.run(update_scores(config, SHEET_ID))
