
class InviteManager():
    def __init__(self):
        self.initialInvites = {}

    async def update_invites(self, guild):
        invites = await guild.invites()
        for invite in invites:
            self.initialInvites[invite.code] = invite.uses
    
    async def on_user_join(self, guild, member, spamChannel, db):
        invites = await guild.invites()
        for invite in invites:
            if self.initialInvites.get(invite.code, 0) != invite.uses:
                await spamChannel.send(f"User **{member}** joined using a invite made by **{invite.inviter}** (Total uses: {invite.uses})")
                await db["user_invites"].insert_one({
                    "user_id": member.id,
                    "inviter_id": invite.inviter.id
                })
                self.initialInvites[invite.code] = invite.uses
                return

    async def get_invites(self, state, message):
        invites = await message.guild.invites()
        if len(invites) == 0:
            await message.channel.send("No active invites found")
        else:
            await message.channel.send(f"Found {len(invites)} active invites")
            msg = ""
            for invite in invites:
                if invite.uses >= 1:
                    msg += f"Invite by `{invite.inviter}` used by `{invite.uses}` people, code=`{invite.code}`\n"
                    if msg.count('\n')>=6:
                        await message.channel.send(msg)
                        msg = ""
            if len(msg) > 0:
                await message.channel.send(msg)

