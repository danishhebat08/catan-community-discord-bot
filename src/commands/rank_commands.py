import io

import discord

import gen_filter


async def leaderboard(state, message):
    words = state.split_words(message.content)
    try:
        lp_filter = await gen_filter.get_read_filter(words[1:], state.db)
    except gen_filter.BadFilterError as ex:
        await message.channel.send(ex.args[0])
        return

    def display_lps(index, i):
        lp, normalized_wins, matches = i["lp"], i["normalized_wins"], i["matches"]
        return (
            f"#{index:>2} {lp:>5}LP {normalized_wins / max(1, matches):>5.3f}x"
            f" {i['user_data'][0].get('display_name', 'unknown')}"
        )

    if "league" not in message.content:
        extra_message = "⚠ Warning ⚠: You are getting the leaderbaord for no specific league. To know if you have a chance" \
                        " to qualify for the EOM tournaments use `!leaderboard ckleague` or `!leaderboard baseleague`.\n"
    else:
        extra_message = ""

    leaders = state.get_leaderboard(lp_filter=lp_filter)
    await message.channel.send(
        extra_message
        + "```\n"
        + "\n".join([display_lps(index, i) async for index, i in leaders])
        + "```\n See <"
        + state.settings["link"]
        + "/leaderboard/month> for more information about what the numbers mean and full ranking",
    )


async def rank(state, message, *extra_words):
    words = tuple(message.content.split()) + extra_words

    if len(message.mentions) >= 2:
        await message.channel.send("Please message one or zero people")
        return
    elif len(message.mentions) == 1:
        if not words[1].startswith("<@") or not words[1].endswith(">"):
            await message.channel.send(
                "Put the user first, and the category afterwards, like this:\n"
                "> !rank @Jas 4p February"
            )
            return
        else:
            user_name = words[1]
            filter_words = words[2:]
            member = message.mentions[0]
    else:
        member = message.author
        user_name = message.author.mention
        filter_words = words[1:]

    user = await state.get_user_by_name(state.normalize_mention(user_name))
    try:
        lp_filter = await gen_filter.get_read_filter(filter_words, state.db)
    except gen_filter.BadFilterError as ex:
        await message.channel.send(ex.args[0])
        return
    if user is None:
        await message.channel.send(
            f"{user_name} has not won any matches yet. My robot senses tell me a comeback is imminent"
        )
        return

    new_lp = (
        await state.db["user_lp"]
            .aggregate(
            [
                {"$match": state.get_lp_filter_for_user(lp_filter, user)},
                {
                    "$group": {
                        "_id": {"user": "$user"},
                        "lp": {"$sum": "$lp"},
                        "matches": {"$sum": "$matches"},
                        "normalized_wins": {"$sum": "$normalized_wins"},
                    }
                },
            ]
        )
            .to_list(1)
    )

    if len(new_lp) > 0:
        lp, matches, normalized_wins = (
            new_lp[0]["lp"],
            new_lp[0]["matches"],
            new_lp[0]["normalized_wins"],
        )
    else:
        lp = matches = normalized_wins = 0

    rank = await state.get_users_ranked_above(lp, lp_filter)

    pronouns = state.get_pronoun(member)
    if rank != 0:
        next_user = await state.get_next_ranked_user(lp, lp_filter)
        if next_user is not None:
            next_user_lp = next_user["lp"]
            extra_message = (
                f" {pronouns[0].capitalize()} needs {next_user_lp - lp} more LP to take the "
                f"#{rank} spot from `{next_user['user_data'][0]['display_name']}`."
            )
        else:
            extra_message = ""
    else:
        extra_message = ""

    if matches != 0 and normalized_wins != 0:
        winrate_message = (
            f" {pronouns[0].capitalize()} wins {normalized_wins / max(1, matches):.3f}x"
            f" as many matches as the average player."
        )
    else:
        winrate_message = ""

    link_text = ""
    if state.settings.get("link", ""):
        link_text = f"\n See <{state.settings['link']}/profile/{member.id}> for full ranking and more info"

    await message.channel.send(
        f"{user['discord_username']}, has gained {lp} LP over {matches}"
        f" matches in this category, putting "
        f"{pronouns[1]} at #{rank + 1}.{extra_message}{winrate_message}{link_text}"
    )


async def admin_lp_list(state, message):
    if not state.is_admin(message.author):
        await message.channel.send("You must be admin to give this commmand")
        return

    await message.channel.send("Preparing files, this may take a while.")
    words = state.split_words(message.content)
    try:
        lp_filter = await gen_filter.get_read_filter(words[1:], state.db)
    except gen_filter.BadFilterError as ex:
        await message.channel.send(ex.args[0])
        return

    users = state.get_leaderboard(1500, lp_filter)

    file_contents = io.BytesIO(
        (
                "Name;Nick;LP;Normalized wins;number of matches\n"
                + "\n".join(
            [
                ";".join(
                    state.extract_nick(
                        user["user_data"][0].get("display_name", "unknown")
                    )
                )
                + "; "
                + str(user["lp"])
                + ";"
                + "{:.3f}".format(
                    user.get("normalized_wins", 0) / max(1, user.get("matches", 1))
                )
                + ";"
                + str(user.get("matches", 0))
                async for rank, user in users
            ]
        )
        ).encode("utf-8")
    )

    await message.channel.send(
        "Here you go:", file=discord.File(file_contents, filename="export.csv")
    )
