from collections import namedtuple

NO_RECENT_MATCH = "NO_RECENT_MATCH"
MALE_ROLE_ID = 894372075949654064
FEMALE_ROLE_ID = 894372075949654063

Setting = namedtuple("Setting", ("regex", "mapping", "message", "default"))

SETTINGS = {
    "min_wager": Setting(r"^\d+$", int, "min_wager must be a number", 25),
    "max_wager": Setting(r"^\d+$", int, "max_wager must be a number", 100),
    "max_wager_2p": Setting(
        r"^\d+$", int, "Max wager for 2 players must be a number", 999
    ),
    "wager_step": Setting(r"^\d+$", int, "wager_step must be a number", 5),
    "link": Setting(r"^https?://.*|^$", str, "link must be a link", ""),
    "report_channel": Setting(
        r"\d{18}", int, "Must be a valid channel number", 816413824163905546
    ),
    "bot_spam_channel": Setting(
        r"\d{18}", int, "Must be a valid channel number", 816413824163905546
    ),
    "first_game_bonus": Setting(r"^\d+$", int, "Must be a valid number", 0),
    "first_win_bonus": Setting(
        r"^\d+$", int, "First game win bonus must be a number", 0
    ),
    "donator_role": Setting(r"^\d+$", int, "Donator must be a role id", 0),
}

match_info_template = """
A {mode} match has been created with id `{id}` {link}
When you actually start the game, please type `!start {id}`
Current players are:

{players}
React with 🎮 to wager {wager} LP and join the game.
"""

game_mode_emoji_mapping = {
    "seafarers": "⛵",
    "c&k": "⚔️",
    "base": "🎲",
    "c&k+seafarers": "🐙",
}

game_mode_synonyms = {
    "sf": "seafarers",
    "ck": "c&k",
    "cities&knights": "c&k",
    "cities&knights+seafarers": "c&k+seafarers",
    "c&k+sf": "c&k+seafarers",
    "c&k&sf": "c&k+seafarers",
    "cksf": "c&k+seafarers",
    **{i: i for i in game_mode_emoji_mapping},
}

game_creation_synonyms = {
    "!base": "base",
    "!sf": "seafarers",
    "!ck": "c&k",
    "!cksf": "c&k+seafarers",
}

months = [
    "january",
    "february",
    "march",
    "april",
    "may",
    "june",
    "july",
    "august",
    "september",
    "october",
    "november",
    "december",
]

periods = ["month", "season", "year", "all"]
match_types = ["mm", "tourney"]

match_type_synonyms = {
    "matchmaker": "mm",
    "tournaments": "tourney",
    "tournament": "tourney",
    **{i: i for i in match_types},
}
