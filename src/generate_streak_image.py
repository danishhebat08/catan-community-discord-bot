import asyncio
import datetime
import io
import os

import aiohttp
import discord
from PIL import Image, ImageDraw, ImageFont

import constants

WIDTH = 512
HEIGHT = 64

day_names = "mo", "tu", "we", "th", "fr", "sa", "su"


async def get_emoji_image(emoji: str):
    # if emoji.startswith("<:"):
    #     emoji_id = int(emoji.split(':')[2][:-1])
    #     emoji: discord.Emoji = discord.utils.get(server.emojis, id=emoji_id)
    #     asset = emoji.url_as(format='png')
    #     return await asset.read()
    if len(emoji) <= 2:
        emoji_hex = hex(ord(emoji[0]))[2:]
        if os.path.exists(f".cache/{emoji_hex}.png"):
            with open(f".cache/{emoji_hex}.png", "rb") as f:
                return f.read()

        async with aiohttp.ClientSession() as ses:
            if not os.path.exists(".cache"):
                os.mkdir(".cache")

            # response = await ses.get(
            #     f'https://emojiapi.dev/api/v1/{}/32.png'
            # )
            response = await ses.get(
                f"https://github.com/twitter/twemoji/raw/master/assets/72x72/{emoji_hex}.png"
            )
            response.raise_for_status()
            data = await response.read()
            with open(f".cache/{emoji_hex}.png", "wb") as f:
                f.write(data)
            return data
    else:
        raise ValueError(f"Invalid emoji {emoji=}")


async def generate_streak_image(days, mode, win_bonus, day_bonus):
    now = datetime.datetime.now()
    img = Image.new("RGBA", (WIDTH, HEIGHT), "#36393f")
    draw = ImageDraw.Draw(img)

    font = ImageFont.truetype("../roboto.ttf", size=18)

    emoji_image_bytes = io.BytesIO(
        await get_emoji_image(constants.game_mode_emoji_mapping[mode])
    )
    emoji_image = (
        Image.open(emoji_image_bytes, formats=("png",))
        .convert("RGBA")
        .resize((32, 32), Image.BICUBIC)
    )
    print(emoji_image, emoji_image.mode)

    img.alpha_composite(
        emoji_image,
        (6, 32),
    )

    draw.rectangle(
        (
            (now.isoweekday()) * WIDTH / (1 + len(days)),
            0,
            (now.isoweekday() + 1) * WIDTH / (1 + len(days)),
            HEIGHT,
        ),
        "#222222",
    )

    for index, (day_name, day) in enumerate(zip(day_names, days)):
        draw.text(
            (int((index + 1.5) * WIDTH / (1 + len(days))), 20),
            day_name,
            anchor="ms",
            align="center",
            stroke_fill="#ffffff",
            font=font,
        )

        if day is None and now.isoweekday() > index:
            draw.text(
                (int((index + 1.5) * WIDTH / (1 + len(days))), 54),
                str(day_bonus + win_bonus),
                anchor="ms",
                fill="#ff0000",
                font=font,
            )
            draw.rectangle(
                (
                    int((index + 1) * WIDTH / (1 + len(days))) + 8,
                    46,
                    int((index + 2) * WIDTH / (1 + len(days))) - 8,
                    48,
                ),
                fill="#ff0000",
            )
        elif day == "full":
            draw.text(
                (int((index + 1.5) * WIDTH / (len(days) + 1)), 54),
                str(day_bonus + win_bonus),
                anchor="ms",
                fill="#00ff00",
                font=font,
            )
        elif day == "half":
            draw.text(
                (int((index + 1.5) * WIDTH / (len(days) + 1)), 54),
                str(day_bonus),
                anchor="ms",
                fill="#ffff00",
                font=font,
            )
    output = io.BytesIO()
    img.save(output, format="PNG")
    output.seek(0)
    return output
    # return io.BytesIO(img.tobytes(encoder_name='png'))


if __name__ == "__main__":
    img = asyncio.run(
        generate_streak_image(
            (False, "half", None, False, True, False, False), "base", 25, 30
        )
    )
    with open("../streak_image.png", "wb") as f:
        f.write(img.read())
