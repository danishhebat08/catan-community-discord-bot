import asyncio

import pymongo

import db


async def fix_broken_tournaments():
    database = db.get_db(False)

    async for user_lp in database["user_lp"].find():
        if user_lp["type"] == "tourney" and user_lp.get("tournament_id") is None:
            if user_lp.get("tournament") is None:
                year = user_lp["year"]
                month = user_lp["month"]

                tournamnent = await database["tournaments"].find_one_and_update(
                    {
                        "name": user_lp["tournament_name"]
                    },
                    {
                        "$set": {
                            "month": month,
                            "year": year,
                            "season": (month - 1) // 3 + 2 + (year - 2021) * 4,
                        }
                    },
                    return_document=pymongo.ReturnDocument.AFTER,
                    upsert=True
                )

                await database["user_lp"].update_one({
                    "_id": user_lp["_id"]
                }, {"$set": {
                    "tournament_id": tournamnent["_id"]
                }})
            else:
                await database["user_lp"].update_one({
                    "_id": user_lp["_id"]
                }, {
                    "$set": {
                        "tournament_id": user_lp["tournament"]
                    }
                })


if __name__ == "__main__":
    asyncio.run(fix_broken_tournaments())
