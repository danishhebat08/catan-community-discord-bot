import asyncio
import json
import aiohttp


async def test_connections():
    with open("../secret.json", "r") as f:
        secrets = json.load(f)

    async with aiohttp.ClientSession(raise_for_status=True) as session:
        result = await session.get(
            "https://discord.com/api/users/mousetail%232544/connections",
            headers={
                "Authorization": "Bearer " + secrets["token"]
            }
        )

        print(await result.text())


if __name__ == "__main__":
    asyncio.run(test_connections())
